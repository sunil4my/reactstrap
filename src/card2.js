import React from 'react';
import {
    Card, CardBody, CardText,
    ListGroup, ListGroupItem, Badge
} from 'reactstrap';
import './styles.css';

const Carddata1 = (props) => {
    return (
        <Card className='profile'>
            <CardBody className='listgroup'>
                <CardText className='para'>My Businesss</CardText>
                <ListGroup>
                    <ListGroupItem>My Connections<Badge color="secondary">04</Badge></ListGroupItem>
                    <ListGroupItem>Recommendations<Badge color="secondary">10</Badge></ListGroupItem>
                    <ListGroupItem>My Pins<Badge color="secondary">08</Badge></ListGroupItem>
                    <ListGroupItem>Pending Request<Badge color="secondary">03</Badge></ListGroupItem>
                </ListGroup>
                <CardText className='para'>Financials</CardText>
                <ListGroup>
                    <ListGroupItem>Payment</ListGroupItem>
                    <ListGroupItem>OutStandings</ListGroupItem>
                    <ListGroupItem>Credit</ListGroupItem>
                </ListGroup>
            </CardBody>
        </Card >
    );
};

export default Carddata1;