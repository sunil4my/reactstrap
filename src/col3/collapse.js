import React, { useState } from 'react';
import {
  Collapse, Card, Button, CardBody
} from 'reactstrap';
import { RiArrowDownSLine } from 'react-icons/ri';
import { AiOutlinePlus } from 'react-icons/ai';


const Collapsedata = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  const [cSelected, setCSelected] = useState([]);

  const onCheckboxBtnClick = (selected) => {
    const index = cSelected.indexOf(selected);
    if (index < 0) {
      cSelected.push(selected);
    } else {
      cSelected.splice(index, 1);
    }
    setCSelected([...cSelected]);
  }

  return (
    <Card className="mb-1">
      <h6 onClick={toggle} style={{ textAlign: "left", padding: "12px" }}>{props.name}<RiArrowDownSLine style={{ float: "right" }} /></h6>
      <Collapse isOpen={isOpen}>
        <CardBody>
          <Button className='filterbutton' onClick={() => onCheckboxBtnClick(1)} active={cSelected.includes(1)}>Product<AiOutlinePlus style={{ float: "right" }}/></Button>
          <Button className='filterbutton' onClick={() => onCheckboxBtnClick(2)} active={cSelected.includes(2)}>Company<AiOutlinePlus style={{ float: "right" }}/></Button>
        </CardBody>
      </Collapse>
    </Card >
  );
}

export default Collapsedata;