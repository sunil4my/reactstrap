import React, { useState } from 'react';
import {
    Collapse, Card, FormGroup, CardBody, Input, Label
} from 'reactstrap';
import { RiArrowDownSLine } from 'react-icons/ri';


const Location = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <Card className="mb-1">
            <h6 onClick={toggle} style={{ textAlign: "left", padding: "12px" }}>{props.name}<RiArrowDownSLine style={{ float: "right" }} /></h6>
            <Collapse isOpen={isOpen}>
                <CardBody>
                    <FormGroup>
                        <Input type="Location" name="Locaton" id="exampleLoaction" placeholder="Enter Location" />
                    </FormGroup>
                </CardBody>
            </Collapse>
        </Card >
    );
}

export default Location;