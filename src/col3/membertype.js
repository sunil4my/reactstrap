import React, { useState } from 'react';
import {
    Collapse, Card, FormGroup, CardBody, Input, Label
} from 'reactstrap';
import { RiArrowDownSLine } from 'react-icons/ri';


const Membertype = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <Card className="mb-1">
            <h6 onClick={toggle} style={{ textAlign: "left", padding: "12px" }}>{props.name}<RiArrowDownSLine style={{ float: "right" }}/></h6>
            <Collapse isOpen={isOpen}>
                <CardBody className='checklist'>
                    <FormGroup check>
                        <Input type="checkbox" name="check" id="exampleCheck" />
                        <Label for="exampleCheck" check>Traders</Label><br></br>
                        <Input type="checkbox" name="check" id="exampleCheck" />
                        <Label for="exampleCheck" check>Manufactures</Label><br></br>
                        <Input type="checkbox" name="check" id="exampleCheck" />
                        <Label for="exampleCheck" check>Retailes</Label><br></br>
                        <Input type="checkbox" name="check" id="exampleCheck" />
                        <Label for="exampleCheck" check>Distributor</Label>
                    </FormGroup>
                </CardBody>
            </Collapse>
        </Card >
    );
}

export default Membertype;