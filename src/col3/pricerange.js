import React, { useState } from 'react';
import {
    Collapse, Card, FormGroup, CardBody, CustomInput, Label
} from 'reactstrap';
import { RiArrowDownSLine } from 'react-icons/ri';


const Pricerange = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <Card className="mb-1">
            <h6 onClick={toggle} style={{ textAlign: "left", padding: "12px" }}>{props.name}<RiArrowDownSLine style={{ float: "right" }}/></h6>
            <Collapse isOpen={isOpen}>
                <CardBody>
                    <FormGroup>
                        <Label for="exampleCustomRange">price</Label>
                        <CustomInput type="range" id="exampleCustomRange" name="customRange" />
                    </FormGroup>
                </CardBody>
            </Collapse>
        </Card >
    );
}

export default Pricerange;