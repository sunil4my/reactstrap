import React from 'react';
import Mediadata from './media.js';


const Mediaprofiledata = (props) => (
    <div>
        {props.profiledata.map(profiledata => <Mediadata {...profiledata} />)}
    </div>
);

export default Mediaprofiledata;