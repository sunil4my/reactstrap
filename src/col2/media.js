import React from 'react';
import {
    Card, CardImg, CardTitle, CardText, CardDeck,
    CardSubtitle, CardBody, Row, Col, Progress
} from 'reactstrap';
import '../styles.css';

const Mediadata = (props) => {
    return (
        <CardDeck>
            <Card className="mb-2">
                <CardBody>
                    <Row>
                        <Col xs="3">
                            <CardImg className='image' src={props.avatar_url} alt="Card image cap" />
                        </Col>
                        <Col xs="9" className='details'>
                            <CardTitle>{props.name}</CardTitle>
                            <CardSubtitle>{props.company}</CardSubtitle>
                            <CardText>This is a wider card with supporting this content is a little bit longer.</CardText>
                            <Progress value={props.value} />
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </CardDeck>
    );
};

export default Mediadata;