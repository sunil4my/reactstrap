import React, { useState } from 'react';
import { RiSearchLine, RiArrowDownSLine } from 'react-icons/ri';
import { GoLocation } from 'react-icons/go';
import {
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    Input,
    Button,
    DropdownToggle,
} from 'reactstrap';
import './styles.css';

const Searchbar = (props) => {
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [splitButtonOpen, setSplitButtonOpen] = useState(false);

    const toggleDropDown = () => setDropdownOpen(!dropdownOpen);

    const toggleSplit = () => setSplitButtonOpen(!splitButtonOpen);

    return (
        <div>
            <br />
            <InputGroup>
                <InputGroupButtonDropdown addonType="prepend" isOpen={splitButtonOpen} toggle={toggleSplit}>
                    <Button outline className='location'><GoLocation />Vizag<RiArrowDownSLine /></Button>
                </InputGroupButtonDropdown>
                <Input placeholder="Search Company/Product" />
                <InputGroupAddon addonType="append"><Button><RiSearchLine /></Button></InputGroupAddon>
            </InputGroup>
        </div>
    );
}


export default Searchbar;