import React from 'react';
import './App.css';
import { Jumbotron, Container, Row, Col } from 'reactstrap';
import Searchbar from './searchbar.js';
import Mediaprofiledata from './col2/mediadata.js';
import Carddata from './card.js';
import Collapsedata from './col3/collapse.js';
import Pricerange from './col3/pricerange.js';
import Membertype from './col3/membertype.js';
import Location from './col3/location.js';
import Carddata1 from './card2.js';



const testData = [
  { name: "Dan Abramov", value: "67", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook" },
  { name: "Sophie Alpert", value: "20", avatar_url: "https://avatars2.githubusercontent.com/u/6820?v=4", company: "Humu" },
  { name: "Sebastian Markbåge", value: "89", avatar_url: "https://avatars2.githubusercontent.com/u/63648?v=4", company: "Facebook" },
];


class App extends React.Component {
  state = { profiles: testData };
  render() {
    return (
      <div className="App" >
        <Jumbotron>
          <Container className="themed-container" fluid={true}>
            <Row>
              <Col sm="12" md={{ size: 6, offset: 3 }}><Searchbar /></Col>
            </Row>
          </Container>
        </Jumbotron>
        <Container>
          <Row>
            <Col sm={{ size: '2.5' }}>
              <Carddata />
              <Carddata1 />
            </Col>
            <Col md={{ size: '6', offset: 1 }}>
              <Mediaprofiledata profiledata={this.state.profiles} />
            </Col>
            <Col md>
              <Collapsedata name="Filter By" />
              <Membertype name='Member Type' />
              <Pricerange name="Price Range" />
              <Location name="Location" />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
