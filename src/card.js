import React from 'react';
import {
    Card, CardImg, CardBody, CardLink,
    CardTitle, CardSubtitle, ListGroup, ListGroupItem, Badge
} from 'reactstrap';
import './styles.css';

const Carddata = (props) => {
    return (
        <Card className='profile'>
            <CardImg className="cardstyle" src="https://www.designhill.com/resize_img.php?atyp=page_file&pth=ft_ca_ct||259||contestfile_1&flp=1549629075-15868770495c5d76930c87e8-24261090.jpg" alt="Card image cap" />
            <CardBody>
                <CardTitle>NION Textiles Pvt Ltd</CardTitle>
                <CardSubtitle>Owner</CardSubtitle>
            </CardBody>
            <hr></hr>
            <CardBody className='listgroup'>
                <ListGroup>
                    <ListGroupItem>My Connections<Badge color="secondary">05</Badge></ListGroupItem>
                    <ListGroupItem>Recommendations<Badge color="secondary">30</Badge></ListGroupItem>
                    <ListGroupItem>My Pins<Badge color="secondary">04</Badge></ListGroupItem>
                    <ListGroupItem>Pending Request<Badge color="secondary">09</Badge></ListGroupItem>
                </ListGroup>
                <CardLink>Grow Your Network</CardLink>
            </CardBody>
        </Card>
    );
};

export default Carddata;