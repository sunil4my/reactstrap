import React, { useState } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    CardImg
} from 'reactstrap';
import './styles.css';
import { RiAccountCircleLine } from 'react-icons/ri';

const Navbars = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar className='navstyle' expand="md">
                <CardImg className="navimg" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAz1BMVEUyNmn8/f6GvFUwNGiJwFQtLWqHvlUvMWmKwlQmK2MsMWYwM2koLWQsK2r7/f4vMGkiJ2EdI18aIF4eJF+BtVcnLGTc3OT3+PoVHFw6RGnv7/MqKGqjo7dgYYbg4Ofn5+xnjWA0OmlMYmY8SGhBQ3J+fpuJiaNYdWN6qlrT09w2Pml1oluAs1jDw9Bqa41NT3qTlKtslF60tMRPZ2Vzn1xhg2FGWWdBUWhycpKrrL7KytVcfGJKX2Zrk15UbmRkiGBAT2hXWIBXcmQOFlpFSHUXAkRXAAAUIklEQVR4nO1da1viPBMupIWeoC0gICCIAoIcRDyCrso+/P/f9PYApW0ySaqtLL7eH57ruXa7IZNJJpPJzB1B+MUv/j9hWqbK/7VqWSfp9SUNoJNpe3lT5vxaNZ+n7YGeao+ShrrO2GgpXGo0NyP747aZdqcSBeo5Embaa7YaUX1uf1nMVHk1/o/A6rgi1uZ1RP9QR1P3y8zyuHQolBdevzNTgba+UPmssf3wJoZh+heg3mw7nmnM4OmHSq3dZ9XSN/YuEZRXu75nWiVAPUpvZC9AD8/HZUoFV4m7zmdGPYX0ifVc80ehajLW6z+I0tTvfqbWL2EC6OZi/0FmdnQqdLfE4l6CRa8cnKpIL83aAQGr9YP18wuwVgERMrXlum7qKrKhKlapH/rLI1yFDjy/JqioxWC96fU2s/m0Efkb4jr991FfZbhQzPSPUoW2Es8wWYruf4qRP27/d+iufhZBc0rDsaqQpEQijnEv3KE++tkqtJU4y0TXHI7O0a5CBxaHEo/scB8Bx0o8srM9BvZKPOZV6MBeiXQ0ju5cGAEqVxmr8Egdtj0YK7Fz5HPUQZ2qxONXIWMlNqzjdWd8IJOixJ+gQqoSO9oPUKGNehuScH7ku/0O+jNZvmLDOnTXEgK4J85/xCp0oPfJq/CHzFEH5JXY+kESEpX4Y1ahA1TGlFj8USokKrGR/xl74Rao1MEM6fGoEOUliamPk0FEwNrxqFBDdw8Pj4LE6LDZCcekOFchOvw45B8rsijK2fPTArUzkZVYEzi6rknNfL6QT6qrnwMyKrmsAzH70KXq0ewEI4scKtSky5dhJTt8lZLr7idgXMlZDzmx8qIZsIwhJbJVqDU//siiPXo5+eqgIkr3uawPefImGeCn+Qa/Cm39/cmJu3Z5ZnRqMMYBCe3xHt41oXWj7JVY61L7jAqX53v5svIVPGzpwxgGJXTm6vhJ0oifItOPnVKP9ki6/pMVA22K54ecptI4LKEj4+0l2ayqve08XVCiM7Z85yH5bAn/FFLrPxvSHzEbhZj7c0o0q2rPdU+XsMuNpO57VowM2mF1aLzgErpbB9EFUJXn1vymDGkQScIDJp/d2sMhJcw/ygQJ7amavRII9gGpigLL133JksZLfjvkpo+6RAmz7tYBmlVSQ5LxUiHJZ7dEt7xpQ6oAEtqza3hRIJtVQjPaVQUaq0ozVQlYaA6xdbOfq/L9B9cSMpovE0i+bG58SFMqCIVb8tTaqjH3whYRFd4muH3Zt3F+WAklojHdK0B8Z/VPu76XYfnsZfg3eZcGKTp3Akj+FZxe2w5e0ptC3Ql1jLLiE+9i5oeyaEx7nDJql+Txz2VzLmTxmt6QdM4YogqjgU8AuRnMc76LL3RNMKY5Ua5MhjbG9w/XDA3Y5y8xl3PHhDxUw7iTVC2DPsWu0xvXe5zyFb0YE6xP8uT99dSQPDDHSXp9v7cHo1KRZZK9Ee/jeTRqfTad9hi/qnjRzc6G5wq6cB85Xcjji6aUjxFgyUsFG82m8fgyFrE1GdNnUzZORuSc0XNz4QUcGmccVR4RYypmaadgOvKS9HgbtavyaxyfrfTccKLNrDuf/Z3mgL0YtYughPK9QIlksJFvfozDlkeOc8Ivzb1+P7MW2P4mpcWqeRG0j0CP5HP2umMgbzyE1Fjhn6SotEuOZ3qyytwPOKw0xnCg/F5C8b35dcuOmhfBZc3vs+nd3T3lgnnpg4R9JUSbWtZjo+lvF/JtMgc56XWvRPEPb5vm2g91MSdpoGzJCRud0QNjhfvtQswN6UFhfkh3uZ2M8h2f3ULWPtDV5qhtCOfZE+pBgt152En4mNhJtfDuh2E/uHw2VPcrqNiW1EU4p3BJszfGm9cb8Ta5MwAqTDwl5vh8NlUNpJQ3uHJYItcMUx2e2flHT4fyU4LBhvyFN2y5Cc8k1XvBG9gF36WPGi76aPdAe4Mut6sw0aP4NhDLNTGsWS3YV85KRnMZktD2b6CRQfmJt1PQjR5SddOyyqZplS1FV1kTaXshIr6wdVgahLLJR5wZq6gX+mf2//ch/8b1THO5U0qYVzVLvbPBcjQaVauj0WreXwslehk0OnXNqfzIMjT7bX4L7hqqEpa/3AJMqmtM4UMOUi3hedGohRurVVtrnXZ68QJArNOloAqR2qMOdxoSIeFumif2yD3my29kCZFiPk9rWFPezG+tofLS3TRl+WzKTTRBgD8/AAkNrEtVsr2RKrbNIzqkSNEGWI5CEKuZBciITrPsKFSwDHU7N2I46vg0heyNsyMStwrdmruHGbqMwGbrhDdkqhOB7KNEtPFpjDwks0XqENHeGE+npJ6UZ2DeZahT5DJhJEzojq5qESrHzmIUvevRBBEPxPMUyYtQlQVLf1uQyoQdEbuvNO9E7xFSHhl3r5EWiBmFRap/E4C5pi7ASJtEq4poOTfmGW4nbM8kDvcEkDNp25sue8dBZehfk9HmigkFUCK3v4pToIKlMflorFkWOUCWwInaOk46LSovySsgFruGQukk/Txlj3BcAW0RZ/ydU1WoxDhWwqNFK1RuUYOu8QUsFu2TNq8W8W3exyLOOlRJK9nHFPZGBGtO+5cgams+Sx85SoTA63a7Aq7p/QH8GxsKkJvPRIPLH6GOH79XCm34wf6sySKqG3iIGVixw7PIWtBaqLHi+cGWqJPUbY08qxRGPR4NLZbPpXYZJZscYbYtwN0wAGI5QTm+GQ2AsRT1DcuN4PdLTzoY9QEOgm3ekwt9CnSGDPWGPbF4S6kce89RaI4PGE/1Ng20lDdkcSwAzkIchVW+6wFf11CNEzdo+d8Kzy7U0HmUqJzxmcMaFkQpcZ2XaKB4JaQTK44lx5ZoUrbUEDAPgqHCamvQ788XHcoKKDbgM2+9wXUYe2bNU1QHXe4IOtgOTff0bkqmrutKWZnRPoM5Fkp8+1DthnFOsZbsRlymGfygqG7gz0fCnilKrc9gowgzt3GtwwzrLknlJJXpLNe4A0LxgxbhiNNJF16w4J6IypysRZlneFdUSaGBqHCj5eykRDqVw5XpiyiBiaqDItJsjRPZ4gmNzCERKecSD+3pfI0g8lXYWR/FcQ0IH/swu/MVc9d3sCSfYC2aKWyMlgOlXqYE4+F1siH8IxP8muY9I8X6bz2fsjelKaGjqATHLabzmWCZ7CwVMhYk44EQpAwW3YmqlM11f8lYT6MuJqLl2Alsjteqy/7mP0th3hbZEkIjSw5ignaJI9ji3GTVjefWiLJzN24iGyNh1tSm87ONvYnxObNIAH6qTV72ICnIivN8gHSl1D0brBwpSdYnEvqJmIlaZ9HflLhUB7SwB3SiqZOtYrETg0XR4Sis3/SnHaIy+0ERy+4+6GV7jVpnPZNXdT7AI2ULcKOgzaUWl95M1cvKZrao4soMenBe8LBWnQ5uTHuz+0TeCHANAEtYAjbwGKEIH8i5hdVnrVFkuws2pa/7z+uuYrFvoAGAmwUoIXRWiHO9EoJtZdWb+TQgZShtQdVV9StVqaBthJIj/oPOCvzBFgJsKeu9gTdl7T0jyar/aHKDDyCICdrer0noNn1ilnuzeWs5T7R2EZQQWFfguv26hA5sK2uaYA3SpwA7beTgC2RoMpnZv8DtjQzsLK6AOumQblUpLGAb/PNvL1uXrv/eRUWkhFjnhAsT+MRew1WudemV4kkDNf9mZaxUhEbNht0r0W4YcZ9G+8hWXozkK0sgIOPWyaCJKhG2jU7sJCJiiRKRwC9ypQc5Jw+vv6sOEWn3spuLGBlTZFKOz7XnkGKowSD8bOGmXYmTy+8RURPcbPpcFlv8ZUqIp5hZBAykQj3BDrAXWYy/zk+KlW8RUUND0cu2xHJd6LGwVUAz9Ng/bkrRqexlmxLzdpJF/nq4TZjFa1noLJDBEJpKC5dUCdu05NUefYMWfQGzuUvctJU6cBgsnF6uU74kubG7qvhcJWUt5k8nfmI+ISEL9Nsy0eAL7YaZdLLYpVqnrUXjsrKrEiCmBFPu/hvhmYe64JcdYtjKLwJMVYvGky9gVn4l7b/wBV80zAuH1skBYWnoV9Gkp0UpIGBWJtZBwMeFaEYdaJWABNHmnoAjNYsqPeYCFVdAWne5Q44uTbGpB9GaAsflYM14SlqULrJB5MgJ2ND5Aj8PAX56EYjRhKriU1mLhbtwdSdUok2OClcJsQSTGP9rAWlbYWaDFLRYeIvU6EJlAjoxA4B060ncMBrQOdCYhH7f1mKyVfqFq2jxKljqUVoQbgaIJ2DS5RP0ZhBWMp6rfCQpYvMKL0CGynVIOf7kDYBwJU68wnHg7/jBiZqciIUXnBQgV4FSc3R8HyBXIuFpDW0w3Uc6x8Y4OS2i5juJ9UAECwWw5EF8q/BQj5wna3BNVgGr+k9QixJRwKx4DxZ7lCLuKRTCjp62ZmCaSP6O1Adbi0lYVAlfg9v24ZKkcCEE+BQQ6oU2jAF8bVjAKMW2IibAmYGucwBhhXgLFhyikNcJJ8iEckUHcC7TrkwV78P46/OUsMT95l/B5pG5F5GS+Rs8i/RhDSIEUjXJF1+dp0gjLHF/jnTB+F4gWEi7s/5v55zSMvWRdA+O8tcpllAXZrqy5wiCRbT620VGq9PxajqKmXYPfg4YNWkMNjEoFwBEiREjIgrwJHFeqMwwqliQ5roHrTJ8UZE3/tAoehKQkGinfREnT/AsUc15jXXL4sQ92meUOz7pckgjWZK5KRdgFB5oPFU58YpCpKD0Fm36a9So15nX4TFAzSuIe8gb4UQolqTHcVYURWDTyIm3lCsTZLJuKDVES3vO38I0bg5X0ziZc2K+cHrxcj+s5FyaI4zNUx6n9Q4HMsYEAR3RRFnOVobnj0kRcdhjaRSM7unj1fmw4rQf1qecIF1ECGHqUIc9zJEsOxn+ebh7Ou0aUsKnYGTLKTWN66e79/HQoa3yFcqiZPvsD265fbZKEyvD8e3Vxce10SxIeS21K1OENIeDXTi9eLgdT7KyS+ZNIRv4yk91x7JjAGyl3b5fPXUdHikjRdEiv+5wzReM64u3l5e/aYVokfT49nb31LV/STJsyQ5B8mlPXBvp/XI+7ygtteZ/8Ytf/OIXv/jFL3CoiklntzpuILN+1mq11sk8fovMUokSuzkAVGuz8K6cVkm8+Kd2l6PRdKb/Mw/N6qWbVY0nPMqJXVF6p08J4XwfkKL3g0kHER4j2vNBUIv7JIYRoYzzm6GWUbToMVQCalz8pT515RxqsTb9C0i71aWWbDJ6TKj1NZ7nFQyQah+yLN+CV+6o+Xo+fIj+bfjSrTY4nMnRS05NNkYuEbwtdOn4xModmV5Yur6VRTEbjWMFUxicxquHmarIPBmQ02CD2aHbJ0zkcw0PdKDCnfvoygSTEJsXi+63PyWoWtoSSHkOp+ht02DE4WXkuRIkaV4OEH6XTCCoaswplEYpQLXWcL5zuNBo9y5bLnd7WTC2IReE8oXu7lUZ/AaQmBzXnjHIt5KDQ2ZJy2EOc4vsb+fE7PjqUnDfE9BOL279V2UmGNUH8Mz1tPctU1UtCwMarQXGF+AzRTvxfzE7HN+PnfsB/w8JrKBQHXttziq1TkA+a7NkkILUIndNSAjlo7nvegSvBCb47ViE/jaANsgFmgz0+sxZfnRGA6wST3qnPrVDInY9IRsx54enmziEbLGATKXPQw+DUfygU9oNIL4KBVqmsT1HWulMVdUSWnxUlnh2UAHO5gA45WmVKbY//py8P+6cjTg5eQj8oegavuUkJfzTU/4djDbJWlXPOeMEqey2QHhBkKZCDh6alhK3lJ4in06nOo6AxLSlPYHPAJEvyOF8eB+NflL+uPUcRz4g8wJ6uQx62QHY8sNYJeOP8/IW+SBehWlP5EcSh1ASrsb1W0vh6xETrsEMYkRmtGgS07awEk0fdT671vh6lMPJrHP2WS4SMQcAAVL+kZCrAhhSV0JecsmvHx31sykXZ5GHIlhIXxjjSqQ85EQlRQtjcfLFKIdaVnrr2XO/tVxMO41GlP8/ApCyg/BcKS2rsEzlKA2jwflAHAyEVFXVFdO06iUTbW5mc3gKwSQUTSw7jZYZytryw1ipyfk4yBFXsXqQhwPHSvMXEQkpq5BrQwyik3gZO7J6ZM8RprBC0SxO6psA9HJbHJwvy8SSsd4nqZHCzGlchFZibkzLmkQQOyEO18wnStGyg/NCVnQTqVL4nZAUKlwSiQWa/seEuhtPmPZi3u/3By3b8i1Wo1G103B45D7BScQBHe8FdbKE8u1Z6dknxELoRuumbiq6w59p2ihbVsmBFZdWihfqTXSiUpnkQgn39Jc57BlCOmzPBb5XNZNDhIq3yCCwCiiRUm/jgbDlr7S0VEVBPbwxV+ldQJq/EuGaqS3wDfHL+/qnoIWXIou133jdVmnKD6yU28iZpsh8jCEtlENDjRN2RLCt8pPvmXeK0SLhQWoRNgZQPjjObFqtwttElisP7Icu1ZvQVtSKQTOYMIKPCBK5ECIwhMfHa44KCYSCAtKJ2tNF0CJwMdqjPO11nP1nZnCFf5qCLwEEgwBwEd4nEGQ+BwIH3wPbvfKnE4GC7PMIcrmxKGnTRdk/LHYS7UfgDNw5bMbJfiHyEsfGbTeNo1Ec7E9yzN0wXrv7SvZEZ/8nUE6nI/v6WZiE/puw4wasJetWIf85HtqTHt+C3YJJdhkKqLvdEGM9hpcKdgsx0d1QcMjAtn7EoTxSHzvWAM73trhhtQ/vz2zheh/FRtJzyTsDFxsHV+HuKBfrRTeuZj0K/Fhv4aUEj3ktcV7c7QmR8wXqdOGQW9DiiJ8Dck+IEO/L9wJ1V+0UhrrkGFPSuxgHALLSSBx0pimFz+UnQH+O8W7ncSLBS7Nf/OIXv/jFL37xP68dj2BsmxLjAAAAAElFTkSuQmCC" alt="Card image cap" />
                <NavbarBrand href="/">CONNETIN</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <NavLink>Messages</NavLink>
                    <Nav className="auto" >
                        <NavItem>
                            <NavLink href="/components/">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#q">Transactions</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#q">Notification</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#q"><RiAccountCircleLine /></NavLink>
                        </NavItem>
                    </Nav>

                </Collapse>
            </Navbar>
        </div>
    );
}

export default Navbars;